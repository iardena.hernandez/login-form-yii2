<?php

use yii\helpers\ArrayHelper;

$params = require __DIR__ . '/params.php';
$result = include __DIR__ . '/common.php';

$result = ArrayHelper::merge($result, [
    'defaultRoute' => '/site',
    'homeUrl' => ['/site'],
    'bootstrap' => [],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'pJs7VgvKfb4ak$aWWxz1W2uQF4cu94y8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => \app\models\activeRecord\User::class,
            'enableAutoLogin' => true,
            'enableSession' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'localhost',
                'username' => null,
                'password' => null,
                'port' => '1025',
                'encryption' => null,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

            ],
        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => \kartik\grid\Module::class
        ]
    ],
    'params' => $params,
]);

if (YII_DEBUG && file_exists(__DIR__ . '/debug.php')) {
    $result = ArrayHelper::merge($result, include(__DIR__ . '/debug.php'));
}


return $result;
