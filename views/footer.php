<?php

use yii\web\View;

/**
 * @var View $this
 */

?>

<footer class="footer">
    <div class="container">
        <p class="pull-right">&copy; Wolfpack IT <?= date('Y') ?></p>
    </div>
</footer>
