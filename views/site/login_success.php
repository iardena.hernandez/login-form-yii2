<?php

/* @var $this yii\web\View */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login Success';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-login-success">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Welcome, <?= \Yii::$app->user->identity->username ?>!</p>
</div>