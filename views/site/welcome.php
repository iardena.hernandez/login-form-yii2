<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Welcome';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-welcome">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        Welcome to Wolfpack!
    </p>
</div>
