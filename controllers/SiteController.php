<?php

namespace app\controllers;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Control use behaviors
     * @return array[]
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays Login page.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->render('login_success', [
                'model' => $model,
            ]);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Displays Login Success page
     *
     * @return string
     */
    public
    function actionLogin_success()
    {
        if (!Yii::$app->user->isGuest) {
            $model = new LoginForm();
            return $this->render('login_success', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays Welcome user
     *
     * @return string
     */
    public
    function actionWelcome()
    {
        return $this->render('welcome');
    }

    /**
     * Displays new user form
     *
     * @return string
     */

    public
    function actionCreate_user()
    {
        if (Yii::$app->user->isGuest) {
            $model = new NewUser();
            return $this->render('create_user', ['model' => $model]);
        }
    }

    public function actionRegister()
    {
        $model = new NewUser();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->username = $_POST['NewUser']['username'];
                $model->email = $_POST['NewUser']['email'];
                $model->password = password_hash($_POST['NewUser']['password'], PASSWORD_ARGON2I);
                $model->authKey = md5(random_bytes((5)));
                $model->accessToken = password_hash(random_bytes(10), PASSWORD_DEFAULT);
                if ($model->save()) {
                    Yii::$app->mailer->compose()
                        ->setFrom('iardena.hernandez@wolfpackit.nl')
                        ->setTo($_POST['NewUser']['email'])
                        ->setSubject('Welcome to Wolfpack!')
                        ->setTextBody('Plain text content')
                        ->setHtmlBody('<b>Welcome, ' . ucwords($_POST['NewUser']['username']) . '!</b>')
                        ->send();
                    return $this->redirect(['site/login']);
                }
                return;
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }
}

